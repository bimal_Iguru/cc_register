﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CC_Register.Model
{
    public class ColorModel
    {
        public string Catagory { get; set; }
        public string BaseClor{ get; set; }

        public string Range { get; set; }

        public string StrValue { get; set; }
    }
}
